<?php

class PagesController {
    
    public function home() {
        return page("index");
    }

    public function report() {
        return page("report");
    }

}