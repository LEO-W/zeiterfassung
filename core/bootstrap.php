<?php

require "controllers/PagesController.php";
require "core/Router.php";

function page($name) {
    return "pages/{$name}.php";
}
